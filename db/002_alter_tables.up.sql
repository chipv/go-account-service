ALTER TABLE `episode_trigger_points` ADD COLUMN `audio_name` VARCHAR(255) AFTER `audio`;

CREATE TABLE IF NOT EXISTS `episode_feedback`
(
    `id`         INT       NOT NULL AUTO_INCREMENT,
    `episode_id` INT       NOT NULL,
    `user_id`    INT       NOT NULL,
    `stars`      INT       NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    FOREIGN KEY (episode_id) REFERENCES episodes (id),
    FOREIGN KEY (user_id) REFERENCES users (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `episodes_completed`
(
    `id`         INT       NOT NULL AUTO_INCREMENT,
    `episode_id` INT       NOT NULL,
    `user_id`    INT       NOT NULL,
    `distance`   FLOAT     NOT NULL,
    `duration`   INT       NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    FOREIGN KEY (episode_id) REFERENCES episodes (id),
    FOREIGN KEY (user_id) REFERENCES users (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
