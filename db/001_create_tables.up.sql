CREATE TABLE IF NOT EXISTS admins
(
    id         SERIAL PRIMARY KEY,
    username   VARCHAR(255) UNIQUE NOT NULL,
    password   VARCHAR(255)        NOT NULL,
    first_name VARCHAR(255)        NOT NULL,
    last_name  VARCHAR(255)        NOT NULL,
    email      VARCHAR(255)        NOT NULL,
    status     BOOL                NOT NULL,
    suspended  BOOL                NOT NULL,
    last_login TIMESTAMP           NULL     DEFAULT NULL,
    created_at TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS users
(
    id         SERIAL PRIMARY KEY,
    username   VARCHAR(255) UNIQUE NOT NULL,
    password   VARCHAR(255)        NOT NULL,
    first_name VARCHAR(255)        NOT NULL,
    last_name  VARCHAR(255)        NOT NULL,
    email      VARCHAR(255)        NOT NULL,
    status     BOOL                NOT NULL,
    suspended  BOOL                NOT NULL,
    activated  BOOL                NOT NULL,
    role_id    INTEGER             NOT NULL,
    last_login TIMESTAMP           NULL     DEFAULT NULL,
    created_at TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE users ADD COLUMN avatar VARCHAR(255) NULL;
ALTER TABLE users ADD COLUMN mobile VARCHAR(15) NULL;
ALTER TABLE users ADD COLUMN dob TIMESTAMP NULL;