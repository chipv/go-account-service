package model

import (
	"database/sql"
	"errors"
	"time"

	"github.com/go-sql-driver/mysql"
)

var (
	ErrUserNotFound = errors.New("user not found")
)

type User struct {
	ID         uint64         `db:"id" json:"id"`
	FacebookID string         `db:"facebook_id" json:"facebookID"`
	Username   sql.NullString `db:"username" json:"username"`
	Email      sql.NullString `db:"email" json:"email"`
	FirstName  sql.NullString `db:"first_name" json:"firstName"`
	LastName   sql.NullString `db:"last_name" json:"lastName"`
	Avatar     sql.NullString `db:"avatar" json:"avatar"`
	Mobile     sql.NullString `db:"mobile" json:"mobile"`
	Dob        mysql.NullTime `db:"dob" json:"dob"`
	Suspended  bool           `db:"suspended" json:"suspended"`
	CreatedAt  time.Time      `db:"created_at" json:"createdAt"`
	UpdatedAt  time.Time      `db:"updated_at" json:"updatedAt"`
}

type UserResponse struct {
	ID          uint64 `json:"id"`
	FacebookID  string `json:"facebookID"`
	Username    string `json:"username"`
	Email       string `json:"email"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Mobile      string `json:"mobile"`
	Avatar      string `json:"avatar"`
	Dob         string `json:"dob"`
	Suspended   bool   `json:"suspended"`
	AccessToken string `json:"accessToken"`
}
