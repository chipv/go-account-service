package model

type Paginator struct {
	PerPage     int64 `json:"perPage"`
	HasPrevious bool  `json:"hasPrevious"`
	HasNext     bool  `json:"hasNext"`
	CurrentPage int64 `json:"currentPage"`
	TotalItems  int64 `json:"totalItems"`
	TotalPages  int64 `json:"totalPages"`
}

const (
	DefaultPerPage int64 = 50
)
