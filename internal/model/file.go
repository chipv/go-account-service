package model

import "mime/multipart"

type File struct {
	File   multipart.File
	Header *multipart.FileHeader
}

type FileResponse struct {
	Name string
	Size int64
	Path string
	URL  string
}
