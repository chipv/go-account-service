package model

import (
	"database/sql"
	"time"
)

type Admin struct {
	ID        uint64         `db:"id" json:"id"`
	Username  string         `db:"username" json:"username"`
	Password  string         `db:"password" json:"password"`
	Email     string         `db:"email" json:"email"`
	Suspended bool           `db:"suspended" json:"suspended"`
	Avatar    sql.NullString `db:"avatar" json:"avatar"`
	CreatedAt time.Time      `db:"created_at" json:"createdAt"`
	UpdatedAt time.Time      `db:"updated_at" json:"updatedAt"`
}

type AdminResponse struct {
	ID          uint64 `json:"id"`
	Username    string `json:"username"`
	Email       string `json:"email"`
	Suspended   bool   `json:"suspended"`
	AccessToken string `json:"accessToken"`
}
