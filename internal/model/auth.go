package model

import (
	"github.com/dgrijalva/jwt-go"
)

type Auth struct {
	User *AuthResponse `json:"user"`
	Role string
}

type AuthResponse struct {
	ID          uint64 `json:"id"`
	FacebookID  string `json:"facebookID"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Username    string `json:"username"`
	Email       string `json:"email"`
	Suspended   bool   `json:"suspended"`
	AccessToken string `json:"accessToken"`
}

var (
	RoleAdmin = "admin"
	RoleUser  = "registered"
)

var supportedRoles = []string{RoleAdmin, RoleUser}

type Claims struct {
	IsAdmin bool
	Role    string
	User    *AuthResponse `json:"user"`
	jwt.StandardClaims
}
