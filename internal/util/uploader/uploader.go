package uploader

import (
	"context"
	"fmt"
	"io"
	"path/filepath"
	"time"

	"github.com/rs/zerolog/log"
	"google.golang.org/api/option"

	"cloud.google.com/go/storage"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

const (
	TypeImage    = "image"
	TypeAudio    = "audio"
	TypeVideo    = "video"
	TypeDocument = "document"
)

type Uploader struct {
	config UploadFileConfig
}

func NewUploader(projectID, bucket, maxSize, fileType, directoryPath string, public bool, timeOut time.Duration) *Uploader {
	return &Uploader{
		config: UploadFileConfig{
			MaxUploadSize: maxSize,
			FileType:      fileType,
			DirectoryPath: directoryPath,
			TimeOut:       timeOut,
			ProjectID:     projectID,
			Bucket:        bucket,
			Public:        public,
		},
	}
}

type FileExtensions map[string][]string

type UploadFileConfig struct {
	ProjectID     string
	Bucket        string
	Public        bool
	MaxUploadSize string
	FileType      string
	DirectoryPath string
	TimeOut       time.Duration
}

func (u Uploader) Upload(ctx context.Context, file *model.File) (*model.FileResponse, error) {
	_, objectAttrs, err := upload(ctx, u.config, file)
	if err != nil {
		switch err {
		case storage.ErrBucketNotExist:
			log.Error().Err(err).Msg("Bucket does not exist. Please create the bucket firstly")
			return nil, errors.Wrap(err, "Bucket does not exist. Please create the bucket firstly")
		default:
			log.Error().Err(err).Msg("invalid bucket name")
			return nil, errors.Wrap(err, "invalid bucket name")
		}
	}

	fileResponse := &model.FileResponse{
		URL:  objectURL(objectAttrs),
		Size: objectAttrs.Size,
		Name: objectAttrs.Name,
	}

	return fileResponse, nil
}

func objectURL(objAttrs *storage.ObjectAttrs) string {
	return fmt.Sprintf("https://storage.googleapis.com/%s/%s", objAttrs.Bucket, objAttrs.Name)
}

func upload(ctx context.Context, config UploadFileConfig, file *model.File) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	r := file.File
	defer file.File.Close()
	credentialsFile, err := filepath.Abs("../running-13209d468de2.json")
	if err != nil {
		return nil, nil, errors.Wrap(err, "cannot get credential file")
	}

	fmt.Println(credentialsFile)
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(credentialsFile))
	if err != nil {
		return nil, nil, err
	}

	bh := client.Bucket(config.Bucket)
	// Next check if the bucket exists
	if _, err = bh.Attrs(ctx); err != nil {
		return nil, nil, err
	}

	obj := bh.Object(file.Header.Filename)
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, r); err != nil {
		return nil, nil, err
	}
	if err := w.Close(); err != nil {
		return nil, nil, err
	}

	if config.Public {
		if err := obj.ACL().Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
			return nil, nil, err
		}
	}

	attrs, err := obj.Attrs(ctx)
	return obj, attrs, err
}
