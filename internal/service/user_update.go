package service

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

type UpdateEpisodeService struct {
	episodeUpdater EpisodeUpdater
}

func NewUpdateEpisodeService(episodeUpdater UserSaver) *UpdateEpisodeService {
	return &UpdateEpisodeService{episodeUpdater: episodeUpdater}
}

func (s *UpdateEpisodeService) Update(ctx context.Context, episode *model.User) error {
	now := time.Now()
	episode.UpdatedAt = now

	err := s.episodeUpdater.Update(ctx, episode)
	return errors.Wrap(err, "cannot update episode.")
}
