package service

import (
	"context"

	"github.com/shinichi2510/internal/model"
)

type Authorizer interface {
	Authorize(ctx context.Context, username, password string) (*model.Admin, error)
}

type UserAuthorizer interface {
	Authorize(ctx context.Context, user *model.User) (*model.User, error)
}

// User Service
type UserGetter interface {
	Get(ctx context.Context, id uint64) (*model.UserResponse, error)
}

type UserWriter interface {
	Create(ctx context.Context, id *model.User) (int64, error)
}

type UserUpdater interface {
	Update(ctx context.Context, user *model.User) error
}

type UserSaver interface {
	UserGetter
	UserUpdater
}

type UserDeleter interface {
	Delete(ctx context.Context, id uint64) error
}
