package service

import (
	"context"

	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

type GetEpisodeService struct {
	episodeReader      EpisodeReader
	triggerPointLister TriggerPointLister
}

func NewGetEpisodeService(episodeReader EpisodeReader, triggerPointLister TriggerPointLister) *GetEpisodeService {
	return &GetEpisodeService{
		episodeReader:      episodeReader,
		triggerPointLister: triggerPointLister,
	}
}

func (s *GetEpisodeService) Get(ctx context.Context, episodeID uint64) (*model.EpisodesResponse, error) {
	episode, err := s.episodeReader.Get(ctx, episodeID)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get story")
	}
	triggerPoints, err := s.triggerPointLister.List(ctx, episodeID)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get story")
	}
	episodeResponse := &model.EpisodesResponse{Episode: episode, TriggerPoints: triggerPoints}
	return episodeResponse, nil
}
