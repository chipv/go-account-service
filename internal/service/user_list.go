package service

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

type ListEpisodeService struct {
	episodeLister      EpisodeLister
	triggerPointLister TriggerPointLister
}

func NewListEpisodeService(episodeLister EpisodeLister, triggerPointLister TriggerPointLister) *ListEpisodeService {
	return &ListEpisodeService{episodeLister: episodeLister, triggerPointLister: triggerPointLister}
}

func (s *ListEpisodeService) List(ctx context.Context, storyID uint64) (*model.ListEpisodeResponse, error) {
	episodes, err := s.episodeLister.List(ctx, storyID)
	if err != nil {
		return nil, errors.Wrap(err, "cannot list episodes.")
	}

	episodesResponse := make([]*model.EpisodesResponse, len(episodes))
	for _, episode := range episodes {
		episodeResponse := &model.EpisodesResponse{Episode: episode}
		triggerPoints, err := s.triggerPointLister.List(ctx, episode.ID)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("cannot list trigger point with episodeID: %d", episode.ID))
		}
		episodeResponse.TriggerPoints = triggerPoints
		episodesResponse = append(episodesResponse, episodeResponse)
	}
	listEpisodeResponse := &model.ListEpisodeResponse{Episodes: episodesResponse}
	return listEpisodeResponse, nil
}
