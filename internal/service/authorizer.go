package service

import (
	"context"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
	"github.com/spf13/viper"
)

type AuthorizeService struct {
	authorizer Authorizer
	sessionTTL time.Duration
}

func NewAuthorizeService(authorizer Authorizer, sessionTTL time.Duration) *AuthorizeService {
	return &AuthorizeService{
		authorizer: authorizer,
		sessionTTL: sessionTTL,
	}
}

func (s *AuthorizeService) Authorize(ctx context.Context, username, password string) (string, error) {
	admin, err := s.authorizer.Authorize(ctx, username, password)
	if err != nil {
		return "", errors.Wrap(err, "cannot authorize user")
	}
	token, err := s.GenerateJWTToken(admin)
	if err != nil {
		return "", errors.Wrap(err, "generate access token failed")
	}
	return token, nil
}

func (s *AuthorizeService) GenerateJWTToken(object *model.Admin) (string, error) {
	expirationTime := time.Now().Add(s.sessionTTL)
	user := &model.AuthResponse{
		ID:        object.ID,
		Email:     object.Email,
		Username:  object.Username,
		Suspended: object.Suspended,
	}
	claims := &model.Claims{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secretKey := []byte(viper.GetString("JWT_SECRET_KEY"))

	accessToken, err := token.SignedString(secretKey)
	if err != nil {
		return "", errors.Wrap(err, "cannot generate access token")
	}

	return accessToken, nil
}
