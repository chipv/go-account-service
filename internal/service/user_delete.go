package service

import (
	"context"

	"github.com/pkg/errors"
)

type DeleteEpisodeService struct {
	episodeReader       EpisodeReader
	triggerPointDeleter EpisodeTriggerPointDeleter
	episodeDeleter      EpisodeDeleter
}

func NewDeleteEpisodeService(episodeReader EpisodeReader, triggerPointDeleter EpisodeTriggerPointDeleter, episodeDeleter EpisodeDeleter) *DeleteEpisodeService {
	return &DeleteEpisodeService{
		episodeReader:       episodeReader,
		triggerPointDeleter: triggerPointDeleter,
		episodeDeleter:      episodeDeleter,
	}
}

func (s *DeleteEpisodeService) Delete(ctx context.Context, id int64) error {
	episode, err := s.episodeReader.Get(ctx, uint64(id))
	if err != nil {
		return errors.Wrap(err, "cannot get episode")
	}
	err = s.triggerPointDeleter.DeleteByEpisodeID(ctx, episode.ID)
	if err != nil {
		return errors.Wrap(err, "cannot delete trigger point by episodeID")
	}
	err = s.episodeDeleter.Delete(ctx, uint64(id))
	return errors.Wrap(err, "cannot delete trigger point")
}
