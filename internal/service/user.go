package service

import (
	"context"
	"database/sql"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
	"github.com/spf13/viper"
)

type AuthorizeUserService struct {
	authorizer UserAuthorizer
	sessionTTL time.Duration
}

func NewAuthorizeUserService(authorizer UserAuthorizer, sessionTTL time.Duration) *AuthorizeUserService {
	return &AuthorizeUserService{
		authorizer: authorizer,
		sessionTTL: sessionTTL,
	}
}

func (s *AuthorizeUserService) Authorize(ctx context.Context, userRequest *model.UserRequest) (*model.UserResponse, error) {
	now := time.Now()
	user := &model.User{
		FacebookID: userRequest.FacebookID,
		FirstName:  sql.NullString{String: userRequest.FirstName, Valid: true},
		LastName:   sql.NullString{String: userRequest.LastName, Valid: true},
		Avatar:     sql.NullString{String: userRequest.Avatar, Valid: true},
		Username:   sql.NullString{String: userRequest.Username, Valid: true},
		Email:      sql.NullString{String: userRequest.Email, Valid: true},
		CreatedAt:  now,
		UpdatedAt:  now,
	}

	user, err := s.authorizer.Authorize(ctx, user)
	if err != nil && err != model.ErrUserNotFound {
		return nil, errors.Wrap(err, "cannot authorize user")
	}

	token, err := s.GenerateJWTToken(user)
	if err != nil {
		return nil, errors.Wrap(err, "generate access token failed")
	}

	userResponse := &model.UserResponse{
		ID:          user.ID,
		AccessToken: token,
		FacebookID:  user.FacebookID,
		FirstName:   user.FirstName.String,
		LastName:    user.LastName.String,
		Avatar:      user.Avatar.String,
		Username:    user.Username.String,
		Email:       user.Email.String,
	}

	return userResponse, nil
}

func (s *AuthorizeUserService) GenerateJWTToken(object *model.User) (string, error) {
	expirationTime := time.Now().Add(s.sessionTTL)
	user := &model.AuthResponse{
		ID:         object.ID,
		FacebookID: object.FacebookID,
		Email:      object.Email.String,
		Username:   object.Username.String,
		FirstName:  object.FirstName.String,
		LastName:   object.LastName.String,
		Suspended:  object.Suspended,
	}
	claims := &model.Claims{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secretKey := []byte(viper.GetString("JWT_SECRET_KEY"))

	accessToken, err := token.SignedString(secretKey)
	if err != nil {
		return "", errors.Wrap(err, "cannot generate access token")
	}

	return accessToken, nil
}
