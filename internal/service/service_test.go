package service_test

import (
	"context"

	"github.com/shinichi2510/internal/model"
	"github.com/stretchr/testify/mock"
)

type StoryMockService struct {
	mock.Mock
}

func (m *StoryMockService) Create(ctx context.Context, article *model.Story) error {
	args := m.Called(ctx, article)
	return args.Error(0)
}

func (m *StoryMockService) Update(ctx context.Context, article *model.Story) error {
	args := m.Called(ctx, article)
	return args.Error(0)
}

func (m *StoryMockService) Get(ctx context.Context, id uint64) (*model.Story, error) {
	args := m.Called(ctx, id)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*model.Story), args.Error(1)
}

func (m *StoryMockService) List(ctx context.Context, paginator *model.Paginator) ([]*model.Story, error) {
	args := m.Called(ctx, paginator)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*model.Story), args.Error(1)
}

func (m *StoryMockService) Delete(ctx context.Context, id uint64) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func buildMockStory(title, synopsis string) *model.Story {
	return &model.Story{
		Title:    title,
		Synopsis: synopsis,
	}
}

type EpisodeMockService struct {
	mock.Mock
}

func (m *EpisodeMockService) Create(ctx context.Context, episode *model.Episode) error {
	args := m.Called(ctx, episode)
	return args.Error(0)
}

func (m *EpisodeMockService) Update(ctx context.Context, episode *model.Episode) error {
	args := m.Called(ctx, episode)
	return args.Error(0)
}

func (m *EpisodeMockService) Get(ctx context.Context, id uint64) (*model.Episode, error) {
	args := m.Called(ctx, id)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*model.Episode), args.Error(1)
}

func (m *EpisodeMockService) List(ctx context.Context, paginator *model.Paginator) ([]*model.Episode, error) {
	args := m.Called(ctx, paginator)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*model.Episode), args.Error(1)
}

func (m *EpisodeMockService) Delete(ctx context.Context, id uint64) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}
