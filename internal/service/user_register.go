package service

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

type CreateEpisodeService struct {
	episodeWriter EpisodeWriter
}

func NewCreateEpisodeService(episodeWriter EpisodeWriter) *CreateEpisodeService {
	return &CreateEpisodeService{episodeWriter: episodeWriter}
}

func (s *CreateEpisodeService) Create(ctx context.Context, episode *model.Episode) error {
	now := time.Now()
	episode.CreatedAt = now
	episode.UpdatedAt = now
	episode.Status = true

	err := s.episodeWriter.Create(ctx, episode)
	return errors.Wrap(err, "cannot create episode.")
}
