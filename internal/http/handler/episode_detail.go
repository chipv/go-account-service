package handler

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/shinichi2510/internal/http/response"
)

type GetEpisodeHandler struct {
	getEpisodeService EpisodeReader
}

func NewGetEpisodeHandler(getEpisodeService EpisodeReader) *GetEpisodeHandler {
	return &GetEpisodeHandler{getEpisodeService: getEpisodeService}
}

func (h *GetEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	episodeID := chi.URLParam(r, "episodeID")
	ID, ok := strconv.ParseInt(episodeID, 10, 64)
	if ok != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, "episodeID invalid")
		return
	}
	episodesResponse, err := h.getEpisodeService.Get(r.Context(), uint64(ID))
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusOK, episodesResponse)
}
