package handler

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/render"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/http/middleware"
	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
)

type CreateCompletedEpisodeHandler struct {
	completedEpisodeWriter CompletedEpisodeWriter
}

func NewCreateCompletedEpisodeHandler(completedEpisodeWriter CompletedEpisodeWriter) *CreateCompletedEpisodeHandler {
	return &CreateCompletedEpisodeHandler{completedEpisodeWriter: completedEpisodeWriter}
}

type CompletedEpisodeRequestParams struct {
	EpisodeID uint64  `json:"episodeID"`
	Duration  uint64  `json:"duration"`
	Distance  float64 `json:"distance"`
}

type CreateCompletedEpisodeRequest struct {
	Params *CompletedEpisodeRequestParams `json:"params"`
}

func (r *CreateCompletedEpisodeRequest) Bind(req *http.Request) error {
	validationError := make([]string, 0)
	if r.Params == nil {
		return errors.Wrap(ErrValidation, "missing 'params' field")
	}

	if r.Params.EpisodeID == 0 {
		validationError = append(validationError, "missing episodeID")
	}
	if r.Params.Duration == 0 {
		validationError = append(validationError, "missing duration")
	}
	if r.Params.Distance == 0 {
		validationError = append(validationError, "missing distance")
	}
	if len(validationError) > 0 {
		return errors.Wrap(ErrValidation, strings.Join(validationError, ","))
	}
	return nil
}

func (h *CreateCompletedEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var req CreateCompletedEpisodeRequest
	err := render.Bind(r, &req)

	errCause := errors.Cause(err)
	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}
	if errCause != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	auth, ok := r.Context().Value(middleware.CtxKeyAuth).(*model.Auth)
	if !ok {
		response.Error(w, r, http.StatusUnauthorized, http.StatusUnauthorized, "invalid auth data")
		return
	}

	fmt.Println("auth: ", auth.User)

	completedEpisode := &model.CompletedEpisode{
		UserID:    auth.User.ID,
		EpisodeID: req.Params.EpisodeID,
		Distance:  req.Params.Distance,
		Duration:  req.Params.Duration,
	}

	err = h.completedEpisodeWriter.Create(r.Context(), completedEpisode)
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusCreated, nil)
}
