package handler

import (
	"context"
	"errors"

	"github.com/shinichi2510/internal/model"
)

var (
	ErrValidation = errors.New("validation error")
)

type StoryWriter interface {
	Create(ctx context.Context, story *model.Story, file *model.File) error
}

type StoryReader interface {
	Get(ctx context.Context, id uint64) (*model.StoryResponse, error)
}

type StoryUpdater interface {
	Update(ctx context.Context, story *model.Story, file *model.File) error
}

type StoryDeleter interface {
	Delete(ctx context.Context, id int64) error
}

type StoryLister interface {
	List(ctx context.Context, page int64, perPage int64) (*model.ListStoriesResponse, error)
}

type Authorizer interface {
	Authorize(ctx context.Context, username, password string) (string, error)
}

type UserAuthorizer interface {
	Authorize(ctx context.Context, user *model.UserRequest) (*model.UserResponse, error)
}

// Episodes
type EpisodeReader interface {
	Get(ctx context.Context, episodeID uint64) (*model.EpisodesResponse, error)
}

type EpisodeLister interface {
	List(ctx context.Context, storyID uint64) (*model.ListEpisodeResponse, error)
}

type EpisodeWriter interface {
	Create(ctx context.Context, story *model.Episode) error
}

type EpisodeDeleter interface {
	Delete(ctx context.Context, id int64) error
}

type EpisodeUpdater interface {
	Update(ctx context.Context, story *model.Episode) error
}

// Trigger Point
type TriggerPointReader interface {
	Get(ctx context.Context, id uint64) (*model.TriggerPoint, error)
}

type TriggerPointWriter interface {
	Create(ctx context.Context, triggerPoint *model.TriggerPoint, file *model.File) error
}

type TriggerPointUpdater interface {
	Update(ctx context.Context, triggerPoint *model.TriggerPoint, file *model.File) error
}

type TriggerPointDeleter interface {
	Delete(ctx context.Context, id int64) error
}

// End run
type CompletedEpisodeWriter interface {
	Create(ctx context.Context, story *model.CompletedEpisode) error
}
