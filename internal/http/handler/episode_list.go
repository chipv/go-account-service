package handler

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/shinichi2510/internal/http/response"
)

type ListEpisodeHandler struct {
	listEpisodeService EpisodeLister
}

func NewListEpisodeHandler(listEpisodeService EpisodeLister) *ListEpisodeHandler {
	return &ListEpisodeHandler{listEpisodeService: listEpisodeService}
}

func (h *ListEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	storyID := chi.URLParam(r, "storyID")
	ID, ok := strconv.ParseInt(storyID, 10, 64)
	if ok != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, "storyID invalid")
		return
	}
	episodesResponse, err := h.listEpisodeService.List(r.Context(), uint64(ID))
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusOK, episodesResponse)
}
