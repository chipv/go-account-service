package handler

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
)

type UpdateEpisodeHandler struct {
	episodeUpdater EpisodeUpdater
}

func NewUpdateEpisodeHandler(episodeUpdater EpisodeUpdater) *UpdateEpisodeHandler {
	return &UpdateEpisodeHandler{episodeUpdater: episodeUpdater}
}

type UpdateEpisodeRequest struct {
	Params *EpisodeRequestParams `json:"params"`
}

func (r *UpdateEpisodeRequest) Bind(req *http.Request) error {
	validationError := make([]string, 0)
	if r.Params == nil {
		return errors.Wrap(ErrValidation, "missing 'params' field")
	}

	if r.Params.Title == "" {
		validationError = append(validationError, "missing Title")
	}
	return nil
}

func (h *UpdateEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	episodeID := chi.URLParam(r, "episodeID")
	ID, ok := strconv.ParseInt(episodeID, 10, 64)
	if ok != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, "episodeID invalid")
		return
	}
	var req CreateEpisodeRequest
	err := render.Bind(r, &req)
	errCause := errors.Cause(err)
	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}
	if errCause != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	episode := &model.Episode{
		ID:          uint64(ID),
		StoryID:     req.Params.StoryID,
		Title:       req.Params.Title,
		Description: sql.NullString{String: req.Params.Description, Valid: true},
		Tags:        sql.NullString{String: req.Params.Tags, Valid: true},
	}

	err = h.episodeUpdater.Update(r.Context(), episode)
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusCreated, nil)
}
