package handler

import (
	"encoding/json"
	"net/http"

	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
)

type AuthorizeHandler struct {
	authorizeService Authorizer
}

func NewAuthorizeHandler(authorizeService Authorizer) *AuthorizeHandler {
	return &AuthorizeHandler{authorizeService: authorizeService}
}

func (h *AuthorizeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	request := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	accessToken, err := h.authorizeService.Authorize(r.Context(), request.Username, request.Password)
	if err != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	response.Success(w, r, http.StatusOK, model.AdminResponse{AccessToken: accessToken})
}
