package handler

import (
	"encoding/json"
	"net/http"

	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
)

type AuthorizeUserHandler struct {
	authorizeService UserAuthorizer
}

func NewAuthorizeUserHandler(authorizeService UserAuthorizer) *AuthorizeUserHandler {
	return &AuthorizeUserHandler{authorizeService: authorizeService}
}

func (h *AuthorizeUserHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	request := &model.UserRequest{}

	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	user, err := h.authorizeService.Authorize(r.Context(), request)
	if err != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	response.Success(w, r, http.StatusOK, user)
}
