package handler

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/shinichi2510/internal/http/response"
)

type DeleteEpisodeHandler struct {
	episodeDeleter EpisodeDeleter
}

func NewDeleteEpisodeHandler(episodeDeleter EpisodeDeleter) *DeleteEpisodeHandler {
	return &DeleteEpisodeHandler{episodeDeleter: episodeDeleter}
}

func (h *DeleteEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	storyID := chi.URLParam(r, "episodeID")
	ID, ok := strconv.ParseInt(storyID, 10, 64)
	if ok != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, "episodeID invalid")
		return
	}
	err := h.episodeDeleter.Delete(r.Context(), ID)
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusOK, nil)
}
