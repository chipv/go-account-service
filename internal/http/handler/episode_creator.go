package handler

import (
	"database/sql"
	"net/http"
	"strings"

	"github.com/go-chi/render"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
)

type CreateEpisodeHandler struct {
	episodeWriter EpisodeWriter
}

func NewCreateEpisodeHandler(episodeWriter EpisodeWriter) *CreateEpisodeHandler {
	return &CreateEpisodeHandler{episodeWriter: episodeWriter}
}

type EpisodeRequestParams struct {
	StoryID     uint64 `json:"storyID"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Tags        string `json:"tags"`
}

type CreateEpisodeRequest struct {
	Params *EpisodeRequestParams `json:"params"`
}

func (r *CreateEpisodeRequest) Bind(req *http.Request) error {
	validationError := make([]string, 0)
	if r.Params == nil {
		return errors.Wrap(ErrValidation, "missing 'params' field")
	}

	if r.Params.Title == "" {
		validationError = append(validationError, "missing Title")
	}
	if len(validationError) > 0 {
		return errors.Wrap(ErrValidation, strings.Join(validationError, ","))
	}
	return nil
}

func (h *CreateEpisodeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var req CreateEpisodeRequest
	err := render.Bind(r, &req)

	errCause := errors.Cause(err)
	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if errCause == ErrValidation {
		response.Error(w, r, http.StatusUnprocessableEntity, http.StatusUnprocessableEntity, err.Error())
		return
	}
	if errCause != nil {
		response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, err.Error())
		return
	}

	episode := &model.Episode{
		StoryID:     req.Params.StoryID,
		Title:       req.Params.Title,
		Description: sql.NullString{String: req.Params.Description, Valid: true},
		Tags:        sql.NullString{String: req.Params.Tags, Valid: true},
	}

	err = h.episodeWriter.Create(r.Context(), episode)
	if err != nil {
		response.Error(w, r, http.StatusInternalServerError, http.StatusInternalServerError, err.Error())
		return
	}

	response.Success(w, r, http.StatusCreated, nil)
}
