package middleware

import (
	"context"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/shinichi2510/internal/http/response"
	"github.com/shinichi2510/internal/model"
	"github.com/spf13/viper"
)

const authenticationKey = "Authorization"

type ctxKey int

const (
	CtxKeyAuth ctxKey = iota
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		notAuth := []string{"/register", "/login", "/facebook/auth"}
		requestPath := r.URL.Path //current request path

		//check if request does not need authentication, serve the request if it doesn't need it
		for _, value := range notAuth {
			if value == requestPath {
				next.ServeHTTP(w, r)
				return
			}
		}

		authHeader := r.Header.Get(authenticationKey)
		if authHeader == "" {
			response.Error(w, r, http.StatusUnauthorized, http.StatusUnauthorized, "missing authentication header")
			return
		}

		authParts := strings.Split(authHeader, " ")
		if len(authParts) != 2 || authParts[0] != "Bearer" {
			response.Error(w, r, http.StatusUnauthorized, http.StatusUnauthorized, "bad authentication header")
			return
		}

		accessToken := authParts[1]
		secretKey := []byte(viper.GetString("JWT_SECRET_KEY"))
		claims := &model.Claims{}
		token, err := jwt.ParseWithClaims(accessToken, claims, func(token *jwt.Token) (interface{}, error) {
			return secretKey, nil
		})

		if token != nil && !token.Valid {
			response.Error(w, r, http.StatusUnauthorized, http.StatusUnauthorized, "token invalid")
			return
		}
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				response.Error(w, r, http.StatusUnauthorized, http.StatusUnauthorized, "signature invalid. Bad access token")
				return
			}
			response.Error(w, r, http.StatusBadRequest, http.StatusBadRequest, "bad token")
			return
		}

		auth := &model.Auth{User: claims.User}
		r = r.WithContext(context.WithValue(r.Context(), CtxKeyAuth, auth))

		next.ServeHTTP(w, r)
	})
}
