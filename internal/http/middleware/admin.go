package middleware

import (
	"net/http"

	"github.com/shinichi2510/internal/model"
)

func Admin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth, ok := r.Context().Value(CtxKeyAuth).(*model.Auth)
		if !ok {

		}
		if auth.Role == model.RoleAdmin {
			next.ServeHTTP(w, r)
		}
	})
}
