package repo_test

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

func TestMain(m *testing.M) {
	viper.SetEnvPrefix("RUNNING_TEST")
	viper.AutomaticEnv()

	zerolog.LevelFieldName = "severity"
	log.Logger = log.With().Caller().Logger()

	os.Exit(m.Run())
}
