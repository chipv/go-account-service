package repo

import (
	"context"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
	"golang.org/x/crypto/bcrypt"
)

type Admin struct {
	db *sqlx.DB
}

func NewAdmin(db *sqlx.DB) *Admin {
	return &Admin{db: db}
}

func (repo *Admin) Create(ctx context.Context, admin *model.Admin) error {
	_, err := repo.db.ExecContext(ctx, "INSERT INTO `admins` (`username`, `email`, `password`, `suspended`, `created_at`, `updated_at`) VALUES(?, ?, ?, ?, ?, ?)", admin.Username, admin.Email, admin.Password, admin.Suspended, admin.CreatedAt, admin.UpdatedAt)
	return errors.Wrap(err, "cannot save new admin to DB")
}

func (repo *Admin) Authorize(ctx context.Context, username, password string) (*model.Admin, error) {
	var admin model.Admin
	err := repo.db.GetContext(ctx, &admin, "SELECT * FROM `admins` WHERE `username` = ?", username)
	if err != nil {
		return nil, errors.Wrap(err, "username is invalid")
	}

	if err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(password)); err != nil {
		return nil, errors.Wrap(err, "cannot authorize user")
	}
	return &admin, nil
}
