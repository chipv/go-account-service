package repo

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/shinichi2510/internal/model"
)

type User struct {
	db *sqlx.DB
}

func NewUser(db *sqlx.DB) *User {
	return &User{db: db}
}

func (repo *User) Authorize(ctx context.Context, user *model.User) (*model.User, error) {
	var userEntity model.User
	err := repo.db.GetContext(ctx, &userEntity, "SELECT * FROM `users` WHERE `facebook_id` = ?", user.FacebookID)

	if err != nil {
		if err == sql.ErrNoRows {
			if err = repo.Create(ctx, user); err != nil {
				return nil, errors.Wrap(err, "cannot create user")
			}
			u, err := repo.GetByFacebookID(ctx, user.FacebookID)
			if err != nil {
				return nil, errors.Wrap(err, "cannot get user")
			}
			return u, nil
		}
		return nil, errors.Wrap(err, "cannot get user")
	}
	return &userEntity, nil
}

func (repo *User) GetByUsernameOrEmail(ctx context.Context, username string) (*model.User, error) {
	var user model.User
	err := repo.db.GetContext(ctx, &user, "SELECT * FROM `users` WHERE `username` = ? OR email = ?", username, username)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get username, user is not exist")
	}
	return &user, nil
}

func (repo *User) Create(ctx context.Context, user *model.User) error {
	_, err := repo.db.ExecContext(ctx, "INSERT INTO `users` (`username`, `email`, `first_name`, `last_name`, `avatar`, `mobile`, `dob`, `suspended`, `activated`, `created_at`, `updated_at`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", user.Username, user.Email, user.FirstName, user.LastName, user.Avatar, user.Suspended, user.FacebookID, user.CreatedAt, user.UpdatedAt)
	return errors.Wrap(err, "cannot save new user to DB")
}
