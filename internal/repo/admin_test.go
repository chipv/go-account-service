package repo_test

import (
	"context"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/shinichi2510/internal/model"
	"github.com/shinichi2510/internal/repo"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/bcrypt"
)

func setupAdminRepo(t require.TestingT) *repo.Admin {
	dbURL := viper.GetString("DB_URL")
	ms, err := sqlx.Open("mysql", dbURL)
	require.NoError(t, err)
	ctx := context.Background()
	_, err = ms.ExecContext(ctx, "TRUNCATE TABLE `admins`")
	require.NoError(t, err)
	return repo.NewAdmin(ms)
}

func TestAdmin_Authorize(t *testing.T) {
	adminRepo := setupAdminRepo(t)
	ctx := context.Background()
	password := "123456"
	admin := buildMockAdmin("admin", "chipv.bka@gmail.com", password)
	admin.ID = 1
	err := adminRepo.Create(ctx, admin)
	require.NoError(t, err)

	actual, err := adminRepo.Authorize(ctx, admin.Username, password)
	assert.NoError(t, err)
	assert.Equal(t, admin.ID, actual.ID)
}

func buildMockAdmin(username, email, password string) *model.Admin {
	now := time.Now()
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return &model.Admin{
		Username:  username,
		Password:  string(hashedPassword),
		Email:     email,
		Suspended: false,
		CreatedAt: now,
		UpdatedAt: now,
	}
}
