package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/shinichi2510/internal/http/handler"
	runningMiddleware "github.com/shinichi2510/internal/http/middleware"
	"github.com/shinichi2510/internal/repo"
	"github.com/shinichi2510/internal/service"
	"github.com/shinichi2510/internal/util/uploader"
	"github.com/spf13/viper"
)

func main() {
	viper.SetEnvPrefix("RUNNING")
	viper.AutomaticEnv()
	dbURL := viper.GetString("DB_URL")

	log.Info().Str("DB_URL", dbURL).Msg("DB_URL")

	logLevelStr := viper.GetString("LOG_LEVEL")
	logLevel, err := zerolog.ParseLevel(logLevelStr)
	if err != nil {
		log.Fatal().Err(err).Msg("Invalid LOG_LEVEL")
	}
	zerolog.LevelFieldName = "severity"
	zerolog.SetGlobalLevel(logLevel)
	log.Logger = log.With().Caller().Logger()

	db, err := sqlx.Connect("mysql", dbURL)

	if err != nil {
		fmt.Println("deo connect duoc den db")
		log.Fatal().Str("dbURL", dbURL).Err(err).Msg("Cannot connect to DB:")
	} else {
		fmt.Println("connected")
	}

	defer func() {
		if err = db.Close(); err != nil {
			log.Error().Err(err).Msg("Cannot close DB connection")
		}
	}()

	var gclProjectID, gclBucket string
	gclPublic := true
	gclProjectID = viper.GetString("GCL_PROJECT_ID")
	gclBucket = viper.GetString("GCL_BUCKET")

	fmt.Println("gclProjectID: ", gclProjectID)
	fmt.Println("gclBucket: ", gclBucket)
	fmt.Println("gclPublic: ", gclPublic)
	fmt.Println("Ping DB: ", db.Ping())

	adminRepo := repo.NewAdmin(db)
	userRepo := repo.NewUser(db)
	sessionTTL := viper.GetDuration("SESSION_TTL")
	authorizeService := service.NewAuthorizeService(adminRepo, sessionTTL)
	userAuthorizeService := service.NewAuthorizeUserService(userRepo, sessionTTL)

	uploader := uploader.NewUploader(gclProjectID, gclBucket, "5MB", "*", "images", gclPublic, 5000000)

	createEpisodeService := service.NewCreateEpisodeService(episodeRepo)
	updateEpisodeService := service.NewUpdateEpisodeService(episodeRepo)
	getEpisodeService := service.NewGetEpisodeService(episodeRepo, triggerPointRepo)
	completedEpisodeService := service.NewCreateCompletedEpisodeService(episodeRepo, completedEpisodeRepo)
	deleteEpisodeService := service.NewDeleteEpisodeService(episodeRepo, triggerPointRepo, episodeRepo)

	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Content-Type", "Token", "Authorization"},
		AllowCredentials: true,
		Debug:            true,
	})

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(cors.Handler)
	r.Use(runningMiddleware.Auth)

	r.Route("/login", func(r chi.Router) {
		r.Method("POST", "/", handler.NewAuthorizeHandler(authorizeService))
	})

	r.Route("/facebook/auth", func(r chi.Router) {
		r.Method("POST", "/", handler.NewAuthorizeUserHandler(userAuthorizeService))
	})

	r.Route("/episodes", func(r chi.Router) {
		r.Method("GET", "/{episodeID}", handler.NewGetEpisodeHandler(getEpisodeService))
		r.Method("POST", "/", handler.NewCreateEpisodeHandler(createEpisodeService))
		r.Method("PUT", "/{episodeID}", handler.NewUpdateEpisodeHandler(updateEpisodeService))
		r.Method("DELETE", "/{episodeID}", handler.NewDeleteEpisodeHandler(deleteEpisodeService))
		r.Method("POST", "/finished", handler.NewCreateCompletedEpisodeHandler(completedEpisodeService))
	})

	err = http.ListenAndServe(":8081", r)
	log.Error().Err(err).Msg("Stopped serving HTTP")
}
